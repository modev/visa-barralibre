# VISA - Barra Libre

## Configuración Firebase
1. Copiar el archivo y cambiar el nombre del archivo `.env.template` por `.env`
```
cp .env.template .env
```

2. Copiar los parámetros del proyecto de firebase en el nuevo archivo `.env`

## Scripts
1. `npm run start` Desarrollo con flujo completo. No tiene .env file configurado
2. `npm run start:completo` Desarrollo con flujo completo. Usa el .env.completo 
3. `npm run start:compra` Desarrollo con flujo de compras. Usa el .env.compra
4. `npm run buildcordova` Compila y lo pasa a carpeta www
4. `npm run build:completo` Compila con flujo de completo y lo pasa a carpeta www
4. `npm run build:compra` Compila con flujo de compra y lo pasa a carpeta www
 


