import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { registerUser, checkIfUserExists } from "../../redux/actions/user";
import ErrorMessage from "../ErrorMessage";
import OndaVisaB from "../../../src/assets/images/Onda-Visa-B.gif";
import OndaVaso from "../../assets/images/10_vaso_150.png";
import Modal from "./modal";

let bluetoothSerial;

class Registro extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
            email: null,
            glassID: null,
            cc: null,
            terms:false,
            modal:false
        };
        this.Continuar = this.Continuar.bind(this)
    }

   componentDidMount(){
        bluetoothSerial = window.bluetoothSerial;
        const self = this;
        if (bluetoothSerial){
            self.cicloLectura = setInterval(
                function(){
                    bluetoothSerial.read(function (data)
                        {   console.log("data",data);
                            if (data === "") return;
                            //toca estar monitoreando el bluetooth hasta
                            // el momento que pongan en RFID en el lector se envia un dato  y podemos cancelar el ciclo
                            console.log("data original: ", data);
                            let content = data;
                            let separador = ",";
                            let response = content.split(separador);

                            let tag_id = response[0];
                            sessionStorage.setItem('tagId', tag_id);
                            let tag_value = response[1];

                            //apenas tengamos el dato podemos cancelar el ciclo
                            clearInterval(self.cicloLectura);
                            
                            self.setState({
                                glassID: tag_id,
                                value_tag: tag_value
                            })

                        },
                        function (b)
                        {
                            console.log("no se puede leer el dispositivo, ", b)
                        }
                    );
                },1000);
        }



    }

    componentWillUnmount() {
        bluetoothSerial = null;
        if (this.cicloLectura) clearInterval(this.cicloLectura);
    }    

    onSubmit = (e) => {
        if(this.state.glassID != null){
            this.props.registerUser(this.state, this.props.history)
        }else{
            alert("Datos Incompletos")
        }
    };

    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    };

    checkTerm = (e) => {
        this.setState({terms:e.target.checked})
    }

    async Continuar(e) {
        if(this.state.name != null && this.state.email != null && this.state.cc != null && this.state.terms){
            if(this.state.name.length >= 8 && this.state.cc.length >= 8){
                const exist = await this.props.checkIfUserExists(this.state.cc);
                if(exist) alert('Usuario registrado');
                else{
                    this.setState({vaso: true});
                }
            }else{
                alert('Su nombre y cedula deben ser mayor a 8 digitos');
            }
        }else{
            alert('Datos incompletos')
        }
    };

    closeModal = (e) => {this.setState({modal:false})};

    render() {
        return (
            <section className="registro">
                {this.state.vaso?
                    <div className="regVaso">
                        <h1 className="titleFormV" >Registrar</h1>
                        <p>Ubica tu vaso en el punto Visa.</p>
                        <button style={{ width:"31%", marginLeft:"-2%", color:"#163048", fontSize:"20px", marginTop:"20.5%" }} className={"submit"} onClick={this.onSubmit}>Registrar</button>
                        <img src={(this.state.glassID&&this.state.glassID.length>0)?OndaVaso:OndaVisaB} alt="Visa" style={{width:"43%", left:"48%", position:"absolute", marginTop:"-45%" }}/>
                    </div>:
                    <div className="form">
                        <h2 className="titleForm" >Llena tus datos para empezar a disfrutar esta noche.</h2>
                        <input type="text" placeholder={"Nombre"} onChange={this.handleChange} name="name" />
                        <input type="email" placeholder={"Correo"} onChange={this.handleChange} name="email" />
                        <input type="number" placeholder={"Cédula"} onChange={this.handleChange} name="cc" />
                        <button className="submit" onClick={this.Continuar}>Registrar</button>
                        <div className="submit" style={{ marginLeft:"83%", marginTop:"-7%", color:"white" }}>
                            <label htmlFor="terminos" onClick={event => this.setState({modal:true})}>Acepta Términos y Condiciones</label>
                            <input style={{marginTop:"-14%" }} type="checkbox" name="terminos" checked={this.state.terms} onChange={this.checkTerm}/>
                        </div>
                    </div>}
                <div>
                    {
                        this.props.error &&
                        <ErrorMessage message={this.props.error} />
                    }
                </div>
                {
                    this.state.modal &&
                        <Modal close={this.closeModal}/>
                }
            </section>
        )
    }
}

const mapStateToProps = state => {
    return {
      user: state.userReducer.user,
      error: state.userReducer.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        registerUser: (user, history) => dispatch(registerUser(user, history)),
        checkIfUserExists: (cc) => (checkIfUserExists(cc))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Registro))
