import React from 'react';

function Modal({ ...props }) {
    const {close} = props;
    return (
        <div className="modal">
            <div className="modal-content">
                <span className="close" onClick={close}>&times;</span>
                <h1 className="m-content">TÉRMINOS Y CONDICIONES ACTIVIDAD “BARRA LIBRE DE EXCUSAS – VISA”</h1>
                <p className="m-content">
                    Por medio del desarrollo de cualquier actividad orientada a la participación en la actividad promocional denominada “BARRA LIBRE DE EXCUSAS – VISA”  (en adelante “la Actividad”), realizado por Visa International Service Association (“Visa”) se da aceptación a los presentes términos y condiciones.

                    CONDICIONES GENERALES DE LA ACTIVIDAD:

                    Podrán participar de la Actividad las personas que hayan cumplido la mayoría de edad en Colombia, titulares de tarjeta VISA emitida por cualquier entidad bancaria en Colombia o en el exterior, que quieran adquirir algunos de los licores ofrecidos por el establecimiento Llorente Restaurante & Bar de la ciudad de Bogotá a través de la aplicación BARRA LIBRE DE EXCUSAS – VISA, que estará instalada en una tableta de propiedad de VISA, ubicada en el establecimiento de comercio.

                    Al participante le será entregado un vaso RFID, a través del cual se realizarán las recargas de dinero para ser utilizado únicamente en el establecimiento Llorente Restaurante & Bar de la ciudad de Bogotá (“El Aliado”), y limitado a los productos de licor establecidos en la aplicación BARRA LIBRE DE EXCUSAS – VISA. El participante deberá asignar un PIN de cuatro números para realizar las recargas de su vaso.

                    La promoción estará vigente del día ___ al ___, y aplica exclusivamente los días martes, entre las 5:00 p.m a 2:30 a.m. del día miércoles.

                    El pago de la recarga deberá realizarse exclusivamente a través de la aplicación BARRA LIBRE DE EXCUSAS – VISA que está en la tableta de propiedad de VISA, ubicada en el establecimiento de comercio, y el valor de la recarga será debitada de la tarjeta VISA inscrita por el participante de la actividad.

                    El establecimiento Aliado se reserva el derecho de admisión.

                    El usuario será el único responsable de los consumos realizados con el vaso que le sea entregado para el consumo de los licores ofrecidos por el establecimiento. El usuario deberá reportar la pérdida, robo o daño del vaso, para que se proceda con la cancelación del vaso y se reemplace por otro.

                    El valor de la recarga debe ser consumido el día en que se realiza la recarga, sin que sea posible acumular el saldo para ser utilizado posteriormente. No hay lugar a la redención en dinero ni en otros bienes, de los saldos no consumidos.

                    El valor de la recarga no incluye propinas del Aliado.

                    VISA en ningún caso será responsable por los daños o perjuicios que pueda sufrir el consumidor de los productos ofrecidos por el Aliado. VISA únicamente actúa como el medio de pago del producto.

                    El exceso de alcohol es perjudicial para la salud. (Ley 30 de 1986)

                    RESPONSABLE DE LA ACTIVIDAD

                    El responsable de la actividad es Visa International Service Association (“Visa”), con su oficina principal de negocios en 900 Metro Center Boulevard, Foster City, California 94404, y con contacto de atención al cliente en Colombia en el correo electrónico SocialSupport@visa.com.
                    CONDICIONES GENERALES DE LA ACTIVIDAD
                    3.1	Visa no asumirá ninguna responsabilidad por el incumplimiento de sus obligaciones, cuando tal incumplimiento total o parcial se produzca por causas o circunstancias constitutivas de fuerza mayor o caso fortuito, hechos de terceros u otros eximentes de responsabilidad calificadas de conformidad con la Ley.
                    3.2	Visa no es un banco o una entidad vigilada por la Superintendencia Financiera de Colombia y no emite tarjetas de débito o crédito. Las instituciones financieras, vigiladas en Colombia por la Superintendencia Financiera, son las instituciones que emiten tarjetas de débito y crédito con la marca Visa. Los consumidores pueden acercarse a la Superintendencia Financiera para tratar asuntos relacionados con su institución financiera.
                    3.3	Responsabilidad por los productos y/o beneficios. El Aliado es exclusiva- y directamente responsable frente a los Tarjetahabientes por la calidad de los productos y/o servicios, así como del cumplimiento de todas las normas existentes y por existir (durante el Término de la Actividad) relacionadas con la producción, normas de sanidad, comercialización y el uso de los productos y/o servicios ofrecidos.
                    3.4	La garantía, calidad, seguridad e idoneidad de los productos y/o servicios adquiridos por los Tarjetahabientes en el establecimiento aliado, es obligación del aliado. Visa no provee producto y/o servicio alguno de los Aliado, ni es su representante, distribuidor, promotor o similar. Por lo tanto, no asume responsabilidad alguna por los productos/servicios comercializados por los Aliado.
                    3.5	Al participar se entenderá que los Tarjetahabientes han aceptado íntegramente estos términos y condiciones, los cuales configuran un contrato entre los Tarjetahabiente y el Organizador. Este documento está a disposición del público a través de la página web
                    3.6	Hasta donde sea permitido por la Ley, Visa se reserva el derecho de suspender temporalmente o de manera indefinida y de forma inmediata la Actividad objeto de los presentes Términos y Condiciones, en caso de detectar o de conocer irregularidades o fraudes en el desarrollo de la misma, o en caso de presentarse algún acontecimiento de fuerza mayor o caso fortuito que afecte en forma grave su ejecución. Estas circunstancias se comunicarán públicamente al consumidor hasta donde sea permitido por la Ley mediante cualquier medio que seleccione Visa o de la misma forma en la que se comunicó públicamente la Actividad y exonerarán de responsabilidad a Visa frente a reclamos originados por la suspensión de la Actividad.
                    3.7	Si alguna disposición de estos Términos y Condiciones se considerara ilegal, nula o por cualquier razón inaplicable, dicha disposición se considerará divisible de estos términos y no afectará la validez y aplicabilidad de las disposiciones restantes.
                    3.8	Si Visa verifica o sospecha que la participación de algún Tarjetahabiente en la Actividad es malintencionada, contraria a la ley, contraria a estos Términos y Condiciones y/o contrarios a la buena fe, hasta donde sea permitido por la Ley procederá a cancelar su participación en la Actividad, y de considerarlo necesario, podrá iniciar las acciones de ley que considere necesarias. Visa se reserva el derecho de solicitar algún comprobante y/o dato adicional para verificar la veracidad de cualquier dato otorgado por el Tarjetahabiente a Visa, así como de suspender temporal o definitivamente su participación en la Actividad mientras sus datos no hayan sido confirmados.
                    3.9	Visa actualizará los presentes Términos y Condiciones cada vez que (i) ingrese o se retire un nuevo establecimiento a la lista de Aliado, por lo tanto, agregará o eliminará el nombre del Aliado, la ciudad en la que se encuentra ubicado el Aliado, la dirección del establecimiento del Aliado,  el horario en el que desarrollará la actividad y las condiciones y restricciones establecidas; (ii) se modifiquen los productos y/o servicios ofrecidos en la Actividad.
                    3.10	Cada Tarjetahabiente deberá obrar de buena fe durante la Actividad.
                    3.11	Los beneficios de la Actividad son otorgados libremente, por esto la Actividad no es una actividad promocional de destreza, de suerte o de azar, por lo tanto, la Actividad no está sujeta a la autorización de ninguna autoridad competente.
                    3.12	Visa al ser un medio de pago y al no tener injerencia alguna sobre los Aliado, no será responsable por la atención otorgada al Tarjetahabiente por parte del personal del Aliado o por la calidad de los productos o servicios entregados por el Aliado durante y con ocasión a la Promoción.
                    3.13	Visa al ser un medio de pago y al no tener injerencia alguna sobre los Aliado, no será responsable por la atención otorgada al Tarjetahabiente por parte del personal del Aliado o por la calidad de los productos o servicios entregados por el Aliado durante y con ocasión a la actividad.
                    3.14	Debido a que Visa no tiene ningún tipo de relación con los Aliado, Visa no será responsable por la realización de reservas, disponibilidad, presentación, sabor, aspecto físico, entre otros, de los productos y servicios ofrecidos por el Aliado.
                    3.15	Debido a que es obligación del Aliado capacitar a todo su personal sobre la Promoción, Visa no es responsable por (i) la forma en la que el personal del Aliado atienda a los Tarjetahabientes, y (ii) el conocimiento del personal del Aliado sobre la promoción.
                    3.16	Todas las controversias que surjan con ocasión a la ejecución de la Actividadserán resueltas de acuerdo con la Ley colombiana ante la jurisdicción ordinaria o ante la jurisdicción la Ley establezca.
                    4. PROTECCIÓN DE DATOS PERSONALES
                    La participación en la Actividad, implicará recolección, procesamiento, almacenamiento, circulación y disposición de la información personal del concursante cuyo tratamiento será realizado conforme a las políticas de protección de datos personales de VISA y del Aliado, las cuales podrá encontrar en los siguientes links      , así como a las siguientes disposiciones específicas:
                    Captura de información

                    La participación en la actividad, así como el desarrollo de sus actividades, contempla la recolección de los siguientes datos personales:

                    Nombre e identificación
                    Número telefónico
                    Correo electrónico
                    Eventualmente fotografías y video

                    Todos los datos personales e información que proporcione el participante, deberán ser veraces, completos, exactos, actualizados, comprobables y comprensibles. Los tipos de datos, finalidades del tratamiento, derechos del titular y canales de contacto se podrán consultar en
                    Uso de información

                    La participación en la actividad requerirá del tratamiento de datos personales de naturaleza general, identificación, ubicación, contenido socioeconómicos.

                    Sin perjuicio de las finalidades del tratamiento previstas en la política de protección de datos personales de VISA y del Aliado, la información personal objeto de tratamiento es utilizado para las siguientes finalidades:
                    Gestionar el envío de mensajes propios de la dinámica de la actividad.
                    Realizar eventuales comunicaciones con fines comerciales, publicitarios, así como noticias o eventos asociados a VISA o del Aliado, y que puedan ser del interés de los usuarios, directamente o través de terceros aliados.
                    Evaluar información y demás datos de la actividad, generando información histórica, estadística, reportes e informes para análisis internos o terceros.
                    Soportar al usuario en la atención de dudas o inquietudes asociadas la actividad.
                    Gestionar la realización de los aspectos administrativos y logísticos asociados a la realización de la actividad.
                    Desarrollar directamente o través de terceros, labores de fidelización y beneficios a clientes actuales y potenciales.

                    Circulación y divulgación de información

                    La información personal del usuario podrá ser transferida y transmitida a terceros países asegurando siempre que el destinatario de la información cuente con niveles adecuados de protección de datos personales acorde con la política de protección de datos de personales de VISA y del Aliado, siempre que esa transferencia se dé en desarrollo de alguna de las finalidades establecidas en los presentes términos y condiciones.
                    La información personal del usuario podrá en algunos casos ser compartidas con terceros socios o aliados de VISA, así como operadores, agentes o proveedores de servicios que estén vinculados a desarrollo de la actividad. En todos los casos, se exigirá a quienes se les comparta sus datos personales, tomar las medidas jurídicas, organizativas y técnicas apropiadas a fin de proteger tus datos personales y respetar la legislación correspondiente. De igual forma, la información personal del usuario podrá eventualmente ser divulgada si ello es necesario para:
                    Dar cumplimiento a una ley u orden administrativa o judicial
                    Impedir o poner fin a un ataque a nuestras redes y nuestros sistemas informáticos
                    Exigir el cumplimiento de los términos y condiciones aplicables a la actividad.

                    Derechos del titular

                    Conforme a las disposiciones normativas y políticas internas del Patrocinador, el concursante tiene derecho a solicitar la actualización, corrección, modificación, supresión de la información personal, así como a solicitar la cancelación de la autorización o parcial del tratamiento de los datos personales siempre que no persista un deber legal o contractual que obligue a su retención.

                </p>
            </div>
        </div>
    );
}

export default Modal;