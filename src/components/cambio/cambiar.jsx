import React, {Component} from "react";
import {withRouter} from "react-router-dom";
import OndaVisaB from "../../assets/images/Onda-Visa-B.gif";
import Activar from "./steps/activar";
import End from "./steps/end";
import ImageEnd from "./steps/imageEnd"
import OndaVaso from "../../assets/images/10_vaso_150.png";
import { updateGlassID } from '../../redux/actions/user';

let bluetoothSerial;

class Cambiar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            step:0,
            glassID:""
        }
    }

    componentDidMount(){
        bluetoothSerial = window.bluetoothSerial;
        const self = this;
        if (bluetoothSerial){
            self.cicloLectura = setInterval(
                function(){
                    bluetoothSerial.read(function (data)
                        {   console.log("data",data);
                            if (data === "") return;
                            //toca estar monitoreando el bluetooth hasta
                            // el momento que pongan en RFID en el lector se envia un dato  y podemos cancelar el ciclo
                            console.log("data original: ", data);
                            let content = data;
                            let separador = ",";
                            let response = content.split(separador);

                            let tag_id = response[0];
                            // let tag_value = response[1];
                             self.setState({
                                glassID: tag_id,
                                // value_tag: tag_value
                            })
                            //apenas tengamos el dato podemos cancelar el ciclo
                            clearInterval(self.cicloLectura);
                            //esto no funciona

                        },
                        function (b)
                        {
                            console.log("no se puede leer el dispositivo, ", b)
                        }
                    );
                },1000);
        }
    }

    handleChange = (e) => {
        this.setState({
            glassID: e.target.value
        })
    }

    nextStep = (step) => {
        this.setState({step},()=>{
            if(step === 1) this.setState({enableCar:true},()=>{
                console.log(this.state , 'setState')
                // this.props.updateGlassID(this.props.userID, this.state.glassID)
            });
            else if(step === 5){
                setTimeout(()=>{
                    this.goPath("/")
                },5000)
            }
        })
    };

    goPath = (path) => {
        this.props.history.push(path)
    };

    render() {
        const content = [
            <Activar nextStep={this.nextStep} glassID = {this.state.glassID} handleChange={this.handleChange}/>,
            <End goPath={this.goPath} nextStep={this.nextStep}/>,
            <ImageEnd nextStep={this.nextStep}/>
        ];
        return (
            <div style={{ display: "flex" }}>
                <div style={{ flex: 1, padding: "10px" }} className="left">
                    {content[this.state.step]}
                </div>
                <div style={{ flex: 2, padding: "10px" }} className="right">
                    <img src={(this.state.glassID&&this.state.glassID.length>0)?OndaVaso:OndaVisaB} alt="Visa" style={{width:"41%", left:"48%", position:"absolute" }}/>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateGlassID: (userID, glassID) => dispatch(updateGlassID(userID, glassID))
    }
}

export default withRouter(Cambiar, mapDispatchToProps)
