import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { loginUser } from '../../redux/actions/user'
import ErrorMessage from "../ErrorMessage";
import logoVisa from "../../assets/images/06_logo_visablanco.png";

class Cambio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cc:""
        }
    }

    onSubmit = e => {
        e.preventDefault();
        if(this.state.cc != null){
            this.props.loginUser(this.state.cc, this.props.history)
        }else{
            alert("Datos Incompletos")
        }
    };

    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    render() {
        return (
            <body className="change">
                <section className="cambio">
                    <div className="textForm">
                        <h2 onClick={this.post}>Vaso perdido</h2>
                        <p>No hay excusa para que no sigas
                            disfrutando la noche.</p>
                        <p>Recupera tu vaso y tu saldo ahora.</p>
                    </div>
                    <form noValidate onSubmit={this.onSubmit}>
                        <input type="text" placeholder={"Cédula"} value={this.state.cc} onChange={this.handleChange} name="cc" />
                        <input type="submit" value={"Continuar"}/>
                        {
                            this.props.error &&
                            <ErrorMessage message={this.props.error} />
                        }
                    </form>

                    <div>
                        <img src={logoVisa} alt="Visa"/>
                    </div>        
                </section>
            </body>
        )
    }
}

const mapStateToProps = state => {
    return {
      error: state.userReducer.error
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        loginUser: (user, history) => dispatch(loginUser(user, history))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Cambio))