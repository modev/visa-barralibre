import React, { Component } from "react";
import { connect } from "react-redux";
import { updateGlassID } from '../../../redux/actions/user'

class Activar extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    handleNextStep = () => {
        console.log('nextstep ' , this.props.glassID)
        this.props.updateGlassID(this.props.userID, this.props.glassID)
        this.props.nextStep(1)
    }

     handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value 
        })
        console.log(e.target.value)
    }


    render() {
        return (
            <React.Fragment>
                <h2>Activar</h2>
                <p>Ahora recargaremos
                    un nuevo vaso con
                    tu saldo anterior.
                </p>
                {/* <input type="text" name="glassID" id="glassID" placeholder="ID del Vaso" value={this.props.glassID} onChange={this.handleChange} /> */}
                <button style={{ marginTop:"52%", width:"92%" }} onClick={this.handleNextStep}>Continuar</button>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userID: state.userReducer.cc
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateGlassID: (userID, glassID) => dispatch(updateGlassID(userID, glassID))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Activar)