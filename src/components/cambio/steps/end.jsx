import React from "react";
import { connect } from "react-redux";

const End = ({...props}) => {
    const {goPath} = props;

    return (
        <React.Fragment>
            <h2>Disfruta tu bebida</h2>
            <p>Tienes ${props.money} disponibles para un próximo trago.</p>
            <button style={{ width:"94%", marginTop:"31%" }} onClick={e=>goPath("/")}>Continuar</button>
        </React.Fragment>
    )
}

const mapStateToProps = (state) => {
    return {
        money: state.userReducer.money
    }
}

export default connect(mapStateToProps)(End)