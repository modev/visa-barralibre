import React from "react";
import { withRouter } from "react-router-dom";

class Splash extends React.Component {

    goPath = () => {
        if(process.env.REACT_APP_FLUJO === "compra") this.props.history.push("/compra");
    };

        goConfig = () => {
        this.props.history.push("/configurar");
    };

    render() {
        return (
            <div className="splash" onClick={this.goPath}>
            < button onClick={this.goConfig} style={{width: '250px', fontSize:'20px', paddingTop:'10px', marginTop : '60%', borderRadius: '20px', paddingLeft: '3px', background: 'transparent', color : 'transparent', border: '1px solid transparent'}} >*</button>
            </div>
        )
    }
}

export default withRouter(Splash)


