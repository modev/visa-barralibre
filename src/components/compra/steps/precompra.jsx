import React from "react";
import OndaVaso from "../../../assets/images/10_vaso_150.png";


export default function PreCompra({...props}) {
    const {saldo,total, sendInfo} = props;
    return (
        <React.Fragment>
          <img src={OndaVaso} alt="Visa" style={{width:"41%", left:"48%", position:"absolute" }}/>

            <h1 style={{width:"92%"}}>Total compra</h1>
            <h3 style={{ fontSize:"36px", marginTop:"-9%" }}>${total}</h3>
            <h1 style={{ width:"92%" }}>Saldo</h1>
            <h3 style={{ fontSize:"36px", marginTop:"-9%" }}>${saldo}</h3>
            <button style={{ marginTop:"15%", width:"92%" }} onClick={e=>{sendInfo()}}>Continuar</button>
        </React.Fragment>
    )
}
