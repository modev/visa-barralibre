import React from "react";

export default function Resumen({...props}) {
    const {list,nextStep} = props;
    // console.log('in resumen', list)

    return (
        <React.Fragment>
            <div className="resumenPedido">
                <div style={{ position:"absolute", top:"11%", left:"7%"}}>
            <h2 style={{ fontSize: "41px", width:"65%" }}>Resumen  de pedido</h2>
            <div className="shop-car-resumen">
                {
                    list.map((item,key)=>(
                        <div key={key} className="car-item-resumen">
                            <div><strong>{item.cant}     {item.name}</strong></div>
                            <br/>
                        </div>
                    ))
                }
            </div>
            <button onClick={e => {nextStep(5)}} style={{ marginTop: "50%", width:"74%" }}>Entregado</button>
            </div>
            </div>
        </React.Fragment>
    )
}
