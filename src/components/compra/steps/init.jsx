import React from "react";

export default function Init({...props}) {
    const {nextStep} = props;
    return (
        <React.Fragment>
            <h2>Compra de bebida</h2>
            <p>Elige tu bebida y brinda
                por una buena excusa.</p>
            <button onClick={e=>{nextStep(1)}}>Continuar</button>
        </React.Fragment>
    )
}
