import React from "react";


export default function Saldo({...props}) {
    const {saldo,allow,nextStep,goPath,money} = props;
    return (
        <React.Fragment>
            {
                allow === 1 ?
                    <React.Fragment>
                        <h2>Disfruta tu bebida</h2>
                        <p>Tienes ${saldo} disponibles para un próximo trago.</p>
                        <button style={{ width:"94%", marginTop:"31%" }} onClick={e=>nextStep(3)}>Continuar</button>
                    </React.Fragment>:
                allow === 2 ?
                    <React.Fragment>
                        <h2 style={{ fontSize: "41px" }}>Has agotado tu saldo</h2>
                        <p style={{ height: "4.6rem" }}>Dirígete al punto de recarga, para continuar disfutando sin excesos.</p>
                        <p style={{ height: "4.6rem",fontWeight:"bold"  }}>Tu saldo actual es: ${money}</p>
                        {/* <button style={{ marginTop:"-5%" }} id="btnRecargar" onClick={e=>goPath("/recarga")}>Recargar</button> */}
                        <button style={{ marginTop:"-6%",width:"92%" }} id="btnRecargar" onClick={e=>goPath("/")}>Salir</button>
                    </React.Fragment>: ''
            }
        </React.Fragment>
    )
}
