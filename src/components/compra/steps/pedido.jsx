import React from "react";
import Cleave from "cleave.js/react";

export default function Pedido({...props}) {
    const {list,total,removeProduct,nextStep} = props;
    console.log(props);
    
    return (
        <React.Fragment>
            <h2 style={{ fontSize: "46px", marginBottom:"10px" }}>Tu Pedido</h2>
            <div className="shop-car">
                {
                    list.map((item,key)=>(
                        <div key={key} className="car-item">
                            <div>
                                <strong style={{ fontWeight:"bold" }} dangerouslySetInnerHTML={{__html: item.name}}></strong>
                                <br/>Cant. {item.cant}
                                <div onClick={e=>removeProduct(key)} style={{ marginLeft: '140px', fontSize: '25px', color: '#152F48',fontWeight:"bold", borderRadius:"62px", border:"solid 1px", height:"32px", width:"33px"}}>&nbsp;&nbsp;-</div>
                                <br/>Total.
                                <Cleave className="cleave" name="price_unit" placeholder="$--" disabled={true}
                                        options={{prefix: "$ ", numeral: true, numeralThousandsGroupStyle: 'thousand',  numeralDecimalMark: '.', delimiter: '.'}}
                                        value={item.number}/>
                            </div>
                        </div>
                    ))
                }
            </div>
                <h3 style={{ margin:"70% 0 0 -4% ", borderTop:"1px solid #C29D5C", padding:"8px",width:"99%",  fontSize:'18px'}}>
                    <div style={{ fontSize:'24px',display:'flex', width:'30px', }}>
                    Total <Cleave className="cleave" name="price_unit" placeholder="$--" disabled={true}
                    options={{prefix: " $ ", numeral: true, numeralThousandsGroupStyle: 'thousand',  numeralDecimalMark: '.', delimiter: '.'}}
                    value={total}/>
                    </div>
                </h3>
                    
            <button style={{ marginTop:"19%" }} onClick={e=>{nextStep(2)}}>Comprar</button>
        </React.Fragment>
    )
}
