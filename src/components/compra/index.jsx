import React, {Component} from "react";
import { connect } from "react-redux";
import {withRouter} from "react-router-dom";
import Init from "./steps/init";
import Pedido from "./steps/pedido";
import Saldo from "./steps/saldo";
import OndaVisaB from "../../assets/images/Onda-Visa-B.gif";
import OndaVaso from "../../assets/images/10_vaso_150.png";
import PreCompra from "./steps/precompra";
import Resumen from "./steps/resumen";
import audioVisa from '../../assets/audio/visa-sound.wav';
import { db } from "../../helpers/firebase";
import End from "./steps/end";
import { checkIfHasMoney, updateMoney, checkUser } from '../../redux/actions/user'
import Rombo from '../../assets/images/rombo.svg';

const products = [
    {name:"Andino Curioso", nameTable:"Andino <br/> Curioso", price:"$24.000",number:24000},
    {name:"Mojito Criollo", nameTable:"Mojito  <br/> Criollo",price:"$21.000",number:21000},
    {name:"Johnnie Walker White Label",nameTable:"Johnnie Walker White Label",price:"$22.000",number:22000},  
    {name:"Trago Beefeater",nameTable:"Trago  <br/> Beefeater",price:"$21.000",number:21000},
    {name:"Cerveza Club Barril",nameTable:"Cerveza Club Barril",price:"$8.000",number:8000}, 
    {name:"Epifanio del Pepino",nameTable:"Epifanio del Pepino",price:"$24.000",number:24000},

];


let bluetoothSerial;

class Compra extends Component {
    constructor(props) {
        super(props);
        this.state = {
            step:0,
            glassID:"",
            allow:0,
            enableCar:false,
            total:'',
            list:[],
            money : null,
            play : false
        }
    }

    componentDidMount(){
        bluetoothSerial = window.bluetoothSerial;
        const self = this;
        if (bluetoothSerial){
            self.cicloLectura = setInterval(
                function(){
                    bluetoothSerial.read(function (data)
                        {   console.log("data",data);
                            if (data === "") return;
                            //toca estar monitoreando el bluetooth hasta
                            // el momento que pongan en RFID en el lector se envia un dato  y podemos cancelar el ciclo
                            console.log("data original: ", data);
                            let content = data;
                            let separador = ",";
                            let response = content.split(separador);

                            let tag_id = response[0];
                            let tag_value = response[1];

                            //apenas tengamos el dato podemos cancelar el ciclo
                            clearInterval(self.cicloLectura);
                            
                            self.setState({
                                glassID: tag_id,
                                value_tag: tag_value
                            })

                        },
                        function (b)
                        {
                            console.log("no se puede leer el dispositivo, ", b)
                        }
                    );
                },1000);
        }

        console.log(this.state.allow)

    }

    nextStep = (step) => {
        this.setState({step},()=>{
            if(step === 5){
                document.body.setAttribute( "style", "-webkit-transform: rotate(0deg);");
                setTimeout(()=>{
                    this.goPath("/")
                },4500)
            }else if(step === 4){
                document.body.setAttribute( "style", "-webkit-transform: rotate(-180deg);");
            }
        })
    };

    goPath = (path) => {
        this.props.history.push(path)
    };

    addProduct = (key) => {
        if(this.state.list.length<=0) this.setState({step:1});
        const shopCar = [...this.state.list];
        const product = Object.assign({},products[key]);
        const index = shopCar.findIndex(item => item.name === product.name);
        if (index < 0 ) {
            product.cant = 1;
            this.setState({list: [...shopCar, product]},this.calcTotal)
        }else{
            shopCar[index].cant += 1;
            shopCar[index].number += product.number;
            this.setState({list: [...shopCar]},this.calcTotal)
        }
    };

    calcTotal = () => {
        if(this.state.list.length > 0 ) {
            const total = this.state.list.reduce((acc,next)=> acc+next.number,0);
            this.setState({total})
        }else{
            this.setState({total:0})
        }
    };

    removeProduct = (key) => {
        const shopCar = [...this.state.list];
        if(shopCar[key].cant >= 2){
            shopCar[key].cant -= 1;
            shopCar[key].number -= shopCar[key].price.replace(/[^0-9]/g,'');
        }else{
            shopCar.splice(key,1);
        }
        this.setState({list: [...shopCar]}, this.calcTotal)
    };

    handleChange = (e) => {
        this.setState({
            glassID: e.target.value
        })
    }

    validateMoney = async () => {
        var user =  await this.props.checkUser(this.state.glassID);
        console.log(this.state.total);
        const hasMoney = await this.props.checkIfHasMoney(this.state.glassID, -(this.state.total));
        console.log(hasMoney);
        if (hasMoney) {
            this.props.updateMoney(this.state.glassID, -(this.state.total));
            console.log('total', this.state.total);
            this.audio = new Audio(audioVisa);
            this.setState((prevState)=>({allow: 1,play : !prevState.play}),() => {
                this.state.play ? this.audio.play() : this.audio.pause();
            });
        } else {
            if(user){
            this.setState({allow: 2, money : user.money})
           }else{
               alert('Vaso no encontrado')
           }
        }
    }   

    sendInfo = async () => {
        var user =  await this.props.checkUser(this.state.glassID)    
        // console.log('function ' , user)
        db.collection('purchases').doc().set({
            list : this.state.list,
            user : user.name,
            cc: user.cc,
            email: user.email
        })        
        console.log('se envio a la BD')
        this.nextStep(4)
    }


    render() {
        const content = [
            <Init nextStep={this.nextStep}/>,
            <Pedido list={this.state.list} total={this.state.total} removeProduct={this.removeProduct} nextStep={this.nextStep}/>,
            <Saldo saldo={this.props.money} allow={this.state.allow} goPath={this.goPath} nextStep={this.nextStep} money={this.state.money}/>,
            <PreCompra saldo={this.props.money} total={this.state.total} nextStep={this.nextStep} sendInfo ={this.sendInfo}/>,
            <Resumen list={this.state.list} nextStep={this.nextStep}/>
        ];
        if(this.state.step === 5) return <End/>;
        return (
            <div style={{ display: "flex" }}>
                <div style={{ flex: 1, padding: "10px" }} className="left">
                    {content[this.state.step]}
                </div>
                <div style={{ flex: 2, padding: "10px" }} className="right">
                    {
                        this.state.step <= 1 ?
                            <div className="productos">
                                {
                                    products.map((product,key)=>(
                                        <div className={`caja caja--${key}`} onClick={e=>this.addProduct(key)}>
                                            <div  className="caja-inner">
                                                <div className="caja-efect">
                                                    <div className="position-text">
                                                        <h2 dangerouslySetInnerHTML={{__html: product.nameTable}}></h2>
                                                        <div className="separator">&nbsp;</div>
                                                        <h4>{product.price}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            {key < 4 && <img className={`rombo rombo--${key}`} src={Rombo} alt=""/>}                                             
                                        </div>

                                    ))
                                }
                            </div>:
                            <div className="simulacion">
                                {
                                    this.state.step === 2 &&
                                    
                                    <React.Fragment>
                                            <div>
                                                <img src={(this.state.glassID&&this.state.glassID.length>0)?OndaVaso:OndaVisaB} alt="Visa" style={{width:"41%", left:"48%", position:"absolute" }}/>
                                                {
                                                    this.state.allow === 0 ?
                                                    <div className="left">
                                                        <h1 style={{ marginLeft:"-52%", width:"48%" }}>Confirma tu pedido</h1> 
                                                        <p style={{ marginLeft:"-52%", width:"53%" }}>Ubica tu vaso en el punto Visa, para disfrutar de las mejores bebidas.</p>
                                                        <button style={{ marginTop:"17%",marginLeft:"-52%", width:"45%" }} onClick={this.validateMoney}>Continuar</button>
                                                    </div>:null
                                                }
                                                                                                                                                                                                        {/*   */}
                                                <input type="text" name="glassID" id="glassID" placeholder="ID del Vaso" onChange={this.handleChange} value={this.state.glassID} style={{ marginLeft:"4%", marginTop: "-8%", display:"none" }}/>
                                                {/* // Este botón valida que el vaso exista
                                                    // Si el vaso NO existe, no pasa nada
                                                    // Si el vaso SI existe,
                                                        // - Se valida si tiene suficiente dinero
                                                            // Si SÍ tiene dinero, se cambia el valor de allow a 1
                                                                // Si tiene dinero, se resta el valor de la BD 
                                                            // Si NO tiene dinero, se cambia el valor de allow a 2 */}
                                            </div>

                                            <button className="con-saldo" onClick={e=>this.setState({allow:1})}>CON SALDO</button>
                                            <button className="sin-saldo" onClick={e=>this.setState({allow:2})}>SIN SALDO</button>
                                    </React.Fragment>
                                }
                            </div>
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        money: state.userReducer.money
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        checkIfHasMoney,
        checkUser,
        updateMoney: (glassID, amount) => dispatch(updateMoney(glassID, amount))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Compra))
