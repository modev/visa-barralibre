import React, { Component } from "react";
import Uno from "./steps/recarga";
import { withRouter } from "react-router-dom";
import Monto from "./steps/monto";
import Tarjeta from "./steps/tarjeta";
import Carga from "./steps/carga";
import SplashEnd from "./steps/splashEnd";
import Carta from "./steps/carta";
import { checkIfHasMoney, updateMoney } from '../../redux/actions/user';
import { connect } from "react-redux";

class Recarga extends Component {
    constructor(props) {
        super(props);
        console.log(props)
        this.state = {
            step: 0,
            carga: null,
            resultadoPago: null
        }
    }

    addZero = (x, n) => {
        while (x.toString().length < n) {
            x = "0" + x;
        }
        return x;
    }

    // Añadir control al elemento "p" principal de la página.
    addControl = () => {
        var d = new Date();
        var h = d.getTime()
        var id = h;
        return (id)
        // console.log('prueba id ' , id)
    }

    nextStep = (step) => {
        if (step === 2) {
            //Hacer petición
        }
        this.setState({ step })
    };

    datafono = (flag, statusApi, totalNumber) => {
        console.log('en el datafono' , totalNumber);
        if (flag){
            this.props.updateMoney(this.props.user.glassID, this.props.money, totalNumber);
            this.setState({ carga: flag, step: 4, status : statusApi })
        }else{
            console.log('en el else',flag);
            this.setState({ carga: flag, step: 4, status : statusApi })
        }
    }

    recibirResultadoPago = (flag) => {
        this.setState({ resultadoPago: flag })
    }

    render() {
        const content = [
            <Carta nextStep={this.nextStep} />,
            <Uno nextStep={this.nextStep} />,
            <Monto nextStep={this.nextStep} datafono={this.datafono} recibirResultadoPago={this.recibirResultadoPago} idGenerate={this.addControl} />,
            <Tarjeta resultadoPago={this.state.resultadoPago} datafono={this.datafono} />,
            <Carga flag={this.state.carga} status={this.state.status} nextStep={this.nextStep} />,
            <SplashEnd nextStep={this.nextStep} />
        ];
        return (
            <div style={{ display: "flex", height: "100%" }}>
                <div style={{ flex: 1, padding: "10px" }} className="">
                    {content[this.state.step]}
                </div>
            </div>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        user: { ...state.userReducer },
        money: state.userReducer.money
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        checkIfHasMoney,
        updateMoney: (glassID, amount, totalNumber) => dispatch(updateMoney(glassID, amount, totalNumber))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Recarga))
