import React, { Component } from "react";
import { connect } from "react-redux";
import { setMoney } from '../../../redux/actions/user'
import { SendPost , getInfo} from '../../../helpers/api.js';
import { db } from "../../../helpers/firebase";
import audioVisa from '../../../assets/audio/visa-sound.wav';

const montoCustomStyles = {
    position: 'absolute',
    width: '100%',
    top: '0',
    left: '0',
    backgroundColor: 'white',
    height: '100%'
}

class Monto extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedAmount: null,
            play : false
        } 
    }

    handleChange = (e) => {
        console.log(e.target.value);
        this.setState({
            selectedAmount: parseInt(e.target.value)
        })
    }

    handleCharge = () => {
        // Esta funcion genera un ID de facturacion 
        var id = this.props.idGenerate() 
        var  id_factura = id.toString()
        console.log('id_factura s' , typeof id_factura) 

        var user = sessionStorage.getItem('user')
        this.props.setMoney(this.state.selectedAmount);
        var totalString = this.state.selectedAmount.toString();
        var totalNumber = '';
        console.log(totalString.length , user)
        if(totalString.length === 6){
            totalNumber = totalString.slice(0 , 3)+'.000'
        }else if(totalString.length === 5){
            totalNumber = totalString.slice(0 , 2)+'.000'
        }else if(totalString.length === 4){
            totalNumber = totalString.slice(0 , 1)+'.000'
        }
        var json_info = {
            email : user,
            monto: this.state.selectedAmount,
            id_factura : id_factura,
            cajero:"01",
            base_dev: "001",
            imp_consumo: 0,
            iva:"0"
        }
        console.log(json_info);
        console.log('slice', totalNumber);        
        SendPost(json_info).then(res => {
            console.log(res.data)
             this.props.recibirResultadoPago( res.data.status)
             if(res.data.cod_aprob === "000000"){
                 res.data.cod_aprob = "Pin invalido o Saldo insuficiente"
             }else{
                 res.data.cod_aprod = "La operacion termina porque se canceló manualmente o sobrepaso el tiempo de espera"
             }
             this.props.datafono(res.data.status==="APPROVED"?true:false, res.data.cod_aprob, totalNumber)
             if(res.data.status === "APPROVED"){
             db.collection('invoices').doc(id_factura).set({ 
                 id_factura : id_factura,
                 status : res.data.status,
                 cod_aprob : res.data.cod_aprob,
                 num_tarjeta : res.data.num_tarjeta,
                 cuenta : res.data.cta,
                 franquicia : res.data.franquicia,
                 monto : res.data.monto,
                 iva : res.data.iva,
                 base_dev : res.data.base_dev,
                 imp_consu : res.data.imp_consu,
                 num_rec : res.data.num_rec
                 })
                this.audio = new Audio(audioVisa)
                this.setState({
                    play : !this.state.play
                }, () => {
                    this.state.play ? this.audio.play() : this.audio.pause();
                })        
            }else{
                db.collection('invoices').doc(id_factura).set({ 
                 id_factura : id_factura,
                 status : res.data.status,
                 cod_aprob : res.data.cod_aprob,
                 })
            }
             
        }).catch(res => {
            console.log(res)
            alert('Error de conexion con el datafono, porfavor volver a conectar');
            sessionStorage.removeItem('user')
            console.log('se elimino la var de ususario ', sessionStorage.getItem('user') )
        })
        this.props.nextStep(3)
    }

    componentDidMount(){
        var user = sessionStorage.getItem('user');
        console.log(user)
    }

    render() {
        return (
            <div className="monto" style={montoCustomStyles}>
                <h1 style={{textAlign: 'center', borderBottom: '1px solid #C29D5C', width: '67%', margin: '14% 29% 7% 19%', fontSize:'3rem', color:'#163048'}}>¿Cuánto quieres recargar hoy?</h1>
                <div className="montos" style={{display: 'flex', flexWrap: 'wrap', marginBottom: '5%', color:'#163048', fontSize:'14px'}}>
                    <label className="check-label" style={{flex: '0 0 50%', display: 'flex', justifyContent: 'center', left:'10%'}}>
                        $25.000
                        <input type="radio" id="required" name="monto" value="25000" onChange={this.handleChange} required={true}/>
                        <span className="checkmark" style={{top: '4px', left: '30%'}}/>
                    </label>
                    <label className="check-label" style={{flex: '0 0 50%', display: 'flex', justifyContent: 'center', left:'-10%'}}>
                        $50.000
                        <input type="radio" id="required" name="monto" value="50000" onChange={this.handleChange} required={true}/>
                        <span className="checkmark" style={{top: '4px', left: '30%'}}/>
                    </label>
                    <label className="check-label" style={{flex: '0 0 50%', display: 'flex', justifyContent: 'center', left:'10%'}}>
                        $100.000
                        <input type="radio" id="required" name="monto" value="100000" onChange={this.handleChange} required={true}/>
                        <span className="checkmark" style={{top: '4px', left: '30%'}}/>
                    </label>
                    <label className="check-label" style={{flex: '0 0 50%', display: 'flex', justifyContent: 'center', left:'-10%'}}>
                        $125.000
                        <input type="radio" id="required" name="monto" value="125000" onChange={this.handleChange} required={true}/>
                        <span className="checkmark" style={{top: '4px', left: '30%'}}/>
                    </label>

                    <input type="number" style={{marginLeft: '29%',
                    width: '45%',
                    padding: '10px',
                    borderRadius: '20px',
                    border: "1px solid rgb(194, 157, 92)",
                    fontSize: "15px",
                    color: "#163048"}} 
                    placeholder="Otro Valor" onChange={this.handleChange}/>

                </div>
                <div className="right" style={{display: 'flex'}}>                
                    <button onClick={this.handleCharge}>Continuar</button>
                    {/* <button onClick={this.prueba}>probar</button> */}
                </div>
            </div>
                
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: {...state.userReducer}
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setMoney: (amount) => dispatch(setMoney(amount))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Monto)


 