import React from "react";

const montoCustomStyles = {
    position: 'absolute',
    width: '100%',
    top: '0',
    left: '0',
    backgroundColor: '#152F48',
    height: '100%'
}

export default function Tarjeta({...props}) {
    const {resultadoPago} = props;
    console.log(resultadoPago);

    return (
        <div className="tarjeta" style={montoCustomStyles} >
             <div className={"tarjeta-box"}/>
        </div>
    )
}
