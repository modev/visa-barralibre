import React, { Component } from 'react'
import { connect } from "react-redux";
import { checkIfGlassExists, setUser } from '../../../redux/actions/user'
import OndaVisaB from "../../../assets/images/Onda-Visa-B.gif";
import OndaVaso from "../../../assets/images/10_vaso_150.png";
import Rombo from '../../../assets/images/rombo.svg';

const products = [
    { name: "Andino Curioso", nameTable: "Andino <br/> Curioso", price: "$24.000", number: 24000 },
    { name: "Mojito Criollo", nameTable: "Mojito  <br/> Criollo", price: "$21.000", number: 21000 },
    { name: "Johnnie Walker White Label", nameTable: "Johnnie Walker White Label", price: "$22.000", number: 22000 },
    { name: "Trago Beefeater", nameTable: "Trago  <br/> Beefeater", price: "$21.000", number: 21000 },
    { name: "Cerveza Club Barril", nameTable: "Cerveza Club Barril", price: "$8.000", number: 8000 },
    { name: "Epifanio del Pepino", nameTable: "Epifanio del Pepino", price: "$24.000", number: 24000 },

];

let bluetoothSerial;

class Carta extends Component {
    constructor(props) {
        super(props)
        this.state = {
            glassID: "",
            user: {},
            id_tag: 0,
            value_tag: 0

        }
        this.handleNextStep = this.handleNextStep.bind(this)
    }



    async findGlass(e) {
        try {
            const user = await this.props.checkIfGlassExists(this.state.glassID)
            if (user) {
                this.props.setUser(user);
                this.setState({ user });
                sessionStorage.setItem('user', user.email)
                console.log('si pasa')
            }
            else {
                this.setState({ error: "Este vaso no existe. Por favor ingresa un ID correcto" })
            }
        } catch (error) {
            this.setState({ error: error })

        }

    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value, error: false
        })
    }

    async handleNextStep() {
        this.props.nextStep(1)
    }

    componentWillUnmount() {
        bluetoothSerial = null;
        if (this.cicloLectura)
            clearInterval(this.cicloLectura);
    }

    render() {
        return (
            <React.Fragment >
                <div className="left">
                    <h1>¿Precios?</h1>
                    <p style={{ width: "30%", height: "7rem" }}>Ve la carta de nuestras
                    bebidas, elige y brinda
                por una buena excusa.</p>
                    <button style={{ position: "absolute", width: "24%", bottom: "80px" }} onClick={this.handleNextStep}>Continuar</button>
                </div>
                <div style={{ flex: 3, padding: "10px" }} className="right">
                    <div className="productos" style={{ marginLeft: 37.3 + '%', marginTop: -13.2 + '%' }}>
                        {
                            products.map((product, key) => (
                                <div className={`caja caja--${key}`}>
                                    <div className="caja-inner">
                                        <div className="caja-efect">
                                            <div className="position-text">
                                                <h2 dangerouslySetInnerHTML={{ __html: product.nameTable }}></h2>
                                                <div className="separator">&nbsp;</div>
                                                <h4>{product.price}</h4>
                                            </div>
                                        </div>
                                    </div>
                                    {key < 4 && <img className={`rombo rombo--${key}`} src={Rombo} alt="" />}
                                </div>

                            ))
                        }
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userID: state.userReducer.cc,
        error: state.userReducer.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        checkIfGlassExists,
        setUser: (user) => dispatch(setUser(user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Carta)
