import React, { Component } from "react";
import { connect } from "react-redux";
import {withRouter} from "react-router-dom";
import CargaExitosa from "../../../assets/images/visa_carga_exitosa.gif"
import CargaFallida from "../../../assets/images/visa-carga-fallida.gif"
import { db } from "../../../helpers/firebase";


class Carga extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userInfo:[]
        }
    }

    componentDidMount() {
        const userInfo = [];
        db.collection("users").where('glassID', "==", this.props.user.glassID).get().then(array => {
            array.forEach(datos => {
                userInfo.push(datos.data());
            });
        })
        this.setState({userInfo},()=>{})
    }

    carga = () => {
        // console.log("funcionoooo")
        this.props.nextStep(5)
        sessionStorage.removeItem('user')
        console.log('se elimino la var de ususario ', sessionStorage.getItem('user') )

    }
    falla= () => {
        // this.props.history.push("/")
        this.props.nextStep(0)
        sessionStorage.removeItem('user')
        console.log('se elimino la var de ususario ', sessionStorage.getItem('user') )
    }


    render(){
        return (
            <div className="carga" onClick={this.props.flag?this.carga:this.falla}>
                 <img src={this.props.flag?CargaExitosa:CargaFallida} alt="Visa" style={{width:"78%", left:"14%", position:"absolute" }}/>
                 <p style={{color:'white', fontSize:'16px', marginTop:'-25px', marginLeft :'58px'}}>{this.props.status}</p>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: {...state.userReducer}
    }
}

export default connect(mapStateToProps, null)(withRouter(Carga))