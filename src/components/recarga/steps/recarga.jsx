import React, { Component } from 'react'
import { connect } from "react-redux";
import { checkIfGlassExists, setUser } from '../../../redux/actions/user'
import OndaVisaB from "../../../assets/images/Onda-Visa-B.gif";
import OndaVaso from "../../../assets/images/10_vaso_150.png";

let bluetoothSerial;

class Uno extends Component {
    constructor(props) {
        super(props)
        this.state = {
            glassID: "",
            user: {},
            id_tag:0,
            value_tag:0

        }
        this.handleNextStep = this.handleNextStep.bind(this)
    }

    componentDidMount(){
        bluetoothSerial = window.bluetoothSerial;
        const self = this;
        if (bluetoothSerial){
            self.cicloLectura = setInterval(
                function(){
                    bluetoothSerial.read(function (data)
                        {
                            if (data === "") return;
                            //toca estar monitoreando el bluetooth hasta
                            // el momento que pongan en RFID en el lector se envia un dato  y podemos cancelar el ciclo
                            console.log("data original: ", data);
                            let content = data;
                            let separador = ",";
                            let response = content.split(separador);

                            let tag_id = response[0];
                            sessionStorage.setItem('tagId', tag_id);
                            // let tag_value = response[1];

                            //apenas tengamos el dato podemos cancelar el ciclo
                            clearInterval(self.cicloLectura);
                            //esto no funciona
                            self.setState({
                                glassID: tag_id
                            })

                        },
                        function (b)
                        {
                            console.log("no se puede leer el dispositivo, ", b)
                        }
                    );
                },1000);
        }
    }

    async findGlass(e) {
        try{
            const user = await this.props.checkIfGlassExists(this.state.glassID)
            if (user) {
                this.props.setUser(user);
                this.setState({user});
                sessionStorage.setItem('user', user.email )
                console.log('si pasa')
            }
            else{
                this.setState({error:"Este vaso no existe. Por favor ingresa un ID correcto"})
            }
        }catch (error) {
            this.setState({error:error})

        }

    }
    handleChange = (e) => {
            this.setState({
                [e.target.name]: e.target.value, error:false
            })
    }

    async handleNextStep() {
        if(this.state.glassID && this.state.user.name){
            this.props.nextStep(2)
        }else{
            this.findGlass();
        }
    }

    componentWillUnmount() {
        bluetoothSerial = null;
        if (this.cicloLectura)
        clearInterval(this.cicloLectura);
    }

    render() {
        return (
            <React.Fragment>
                <div  className="left">
                    <h1>Recarga</h1>
                    <p style={{ width: "30%", height:"7rem" }}>Si crees que aún la noche
                    es joven, es tiempo de
                    hacer una recarga.</p>
                    {
                        this.state.user.name &&
                            <React.Fragment>
                                <div style={{ fontWeight:"bold" }}>Nombre: <b>{this.state.user.name}</b></div>
                                <div style={{ fontWeight:"bold" }}>Saldo Disponible: <b>$ {this.state.user.money}</b></div>
                            </React.Fragment>
                    }
                    {
                        this.state.error &&
                            <div style={{ width:"30%"}}>
                                {this.state.error}
                            </div>
                    }
                    <button style= {{ position:"absolute",width:"24%", bottom:"80px"}} onClick={this.handleNextStep}>Continuar</button>
                </div>
                <div style={{ flex: 3, padding: "10px" }} className="right">
                    <img src={(this.state.glassID&&this.state.glassID.length>0)?OndaVaso:OndaVisaB} alt="Visa" style={{width:"43%", left:"48%", position:"absolute", top:"14%"}}/>
                    <div style={{ marginTop:"-11%" }}>
                    {/*  */}
                        <input type="text" style={{ display:"none" }}  name="glassID" id="glassID" placeholder="ID del Vaso" value={this.state.glassID} onChange={this.handleChange} />
                    </div>
                </div>
        </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userID: state.userReducer.cc,
        error: state.userReducer.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        checkIfGlassExists,
        setUser: (user) => dispatch(setUser(user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Uno)
