import React from "react";
import { withRouter } from "react-router-dom";
import { db } from "../../helpers/firebase";
import XLSX from "xlsx";


class Configurar extends React.Component {

    constructor() {
        super()
        this.state = {
            ip : '',
            list : []
        }
    }

    componentDidMount(){
        const ip = sessionStorage.getItem('ip');
        if(ip){
            this.setState({ip});
            console.log("ip: ",ip);
        }else{
            console.log('no hay ip aun');
        }
        const listArray = [];
        db.collection('purchases').get().then(Query_array => {
            Query_array.forEach(element => {
                listArray.push({user:element.data().user,list:element.data().list})
            });
            this.setState({list:listArray})
        });
    }

    goPath = () => {
        this.props.history.push("/");
    };

    Continuar = () => {
       sessionStorage.removeItem('ip');
       sessionStorage.setItem('ip', this.state.ip);
       this.goPath()
    };

    onChange = (e) => {
        this.setState({ip:e.target.value})
    };

    report = () => {
        const ws = XLSX.utils.table_to_sheet(document.getElementById("sheetjs"));
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Reporte");
        XLSX.writeFile(wb, `reporte_visa_barralibre.xls`);
    };
  
    render() {
        return (
            <div style={{marginTop : '80px'}}>
                <p>Configuración</p>
                <input id="inp_config" placeholder={this.state.ip} onChange={this.onChange}/>
                <button className="submit" onClick={this.Continuar}>Cambiar IP</button>
                <br/>
                <button onClick={this.report}>Reporte</button>
                <div style={{display:"none"}}>
                    <table id={"sheetjs"}>
                        <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Precio U</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.list.map((item,key) => {
                                return <React.Fragment>
                                    <tr key={key}>
                                        <td rowSpan={item.list.length}>{item.user}</td>
                                        <td>{item.list[0]?item.list[0].name:''}</td>
                                        <td>{item.list[0]?item.list[0].number:''}</td>
                                        <td>{item.list[0]?item.list[0].cant:''}</td>
                                        <td>{item.list[0]?item.list[0].price:''}</td>
                                    </tr>
                                    {
                                        item.list.slice(1,item.list.length).map((product,idx)=>{
                                            return <tr key={idx}>
                                                <td>{product.name}</td>
                                                <td>{product.number}</td>
                                                <td>{product.cant}</td>
                                                <td>{product.price}</td>
                                            </tr>
                                        })
                                    }
                                </React.Fragment>
                            })
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default withRouter(Configurar)


