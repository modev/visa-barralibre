import React, {Component} from "react";
import {withRouter} from "react-router-dom";
import {slide as Menu} from "react-burger-menu";

class SideMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuOpen: false
        }
    }

    handleStateChange (state) {
        this.setState({menuOpen: state.isOpen})
    }

    transition = (path) => {
        this.setState({menuOpen:false},()=>{
            this.props.history.push(path)
        })
    };

    render() {
        return (
            <Menu className="bm-item-list" isOpen={this.state.menuOpen} onStateChange={(state) => this.handleStateChange(state)}>
                <ul>
                    <li className="menu-item" onClick={e=>this.transition("/registro")}><span>Registro</span></li>
                    <li className="menu-item" onClick={e=>this.transition("/recarga")}><span>Recarga</span></li>
                    <li className="menu-item" onClick={e=>this.transition("/compra")}><span>Compra</span></li>
                    <li className="menu-item" onClick={e=>this.transition("/cambio")}><span>Cambio</span></li>
                    <li className="menu-item" onClick={e=>this.transition("/saldo")}><span>Saldo</span></li>
                </ul>
            </Menu>
        )
    }
}

export default withRouter(SideMenu)
