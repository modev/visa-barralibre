import React from 'react'
import PropTypes from 'prop-types'

const ErrorMessage = (props) => {
  return (
    <div style={{color:"#C29D5C", marginLeft:"47%"}}>
      {props.message}
    </div>
  )
}

ErrorMessage.propTypes = {
  message: PropTypes.string
}

export default ErrorMessage