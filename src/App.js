import React from "react";
//Libraries

// BrowserRouter no funciona esa mierda, no deja ver las imagenes
import { HashRouter as Router, Route } from "react-router-dom";
import SideMenu from "./components/side";
import Registro from "./components/registro";
import Recarga   from "./components/recarga";
import Compra from "./components/compra";
import Splash from "./components/splash";
import Configurar from "./components/configurar"; 
import Cambio from "./components/cambio";
import Cambiar from "./components/cambio/cambiar";
import Saldo from "./components/saldo";

//Redux
import { Provider } from 'react-redux'
import configureStore from './redux/configureStore'

function App() {
  return (
    <Provider store={configureStore()}>      
        <Router>
          {process.env.REACT_APP_FLUJO === "completo" && <SideMenu />}
          <Route exact path="/" render={() => <Splash />} />
          <Route exact path="/cambio" render={() => <Cambio />} />

          <div id="contenido">        
            <Route exact path="/registro" render={() => <Registro />} />
            <Route exact path="/recarga" render={() => <Recarga />} />
            <Route exact path="/compra" render={() => <Compra />} />
            <Route exact path="/cambiar" render={() => <Cambiar />} />
            <Route exact path="/configurar" render={() => <Configurar />} />
            <Route exact path="/saldo" render={() => <Saldo />} />
          </div>
        </Router>
    </Provider>
  );
}

export default App;
