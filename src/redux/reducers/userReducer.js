import { SET_USER, LOGOUT_USER, SET_MONEY, SET_GLASS_ID, SET_ERROR } from "../actions/user";

const initialState = {
  name: null,
  email: null,
  cc: null,
  money: null,
  glassID: null,
  error: null
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ERROR: 
      return {
        ...state,
        error: action.payload.error 
      }
    case SET_USER:      
      return {
        ...state,
        ...action.payload.user        
      };

    case SET_MONEY: {
      return {
        ...state,
        money: action.payload.money
      };
    }

    case SET_GLASS_ID: {
      return {
        ...state,
        glassID: action.payload.glassID
      }
    }

    case LOGOUT_USER:
      return initialState;

    default:
      return state;
  }
};

export default userReducer;
