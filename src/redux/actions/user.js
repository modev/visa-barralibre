import { db } from "../../helpers/firebase";

export const SET_USER = "@visa-barralibre/user/SET_USER";
export const LOGOUT_USER = "@visa-barralibre/user/LOGOUT_USER";
export const SET_MONEY = "@visa-barralibre/user/SET_MONEY";
export const SET_GLASS_ID = "@visa-barralibre/user/SET_GLASS_ID";
export const SET_ERROR = "@visa-barralibre/user/SET_ERROR";

let bluetoothSerial;


const setError = error => {
  return {
    type: SET_ERROR,
    payload: {
      error
    }
  };
};

export const checkIfUserExists = userID => {
  return new Promise((resolve, reject) => {
    db.collection("users")
      .doc(userID)
      .get()
      .then(doc => {
        const exists = doc.exists;
        if (exists) {
          resolve(doc.data());
        } else {
          resolve(exists);
        }
      })
      .catch(error => reject(error));
  });
};

export const loginUser = (cc, history) => async dispatch => {
  const exists = await checkIfUserExists(cc);

  if (exists) {
    dispatch(setUser(exists));
    history.push("/cambiar");
  } else {
    dispatch(setError("Este usuario no existe."));
  }
};

export const logoutUser = () => {
  return {
    type: LOGOUT_USER
  };
};

export const checkIfGlassExists = glassID => {
  return new Promise((resolve, reject) => {
    db.collection("users")
      .where("glassID", "==", glassID)
      .get()
      .then(function(querySnapshot) {
        let results = [];
        if (!querySnapshot.empty) {
          querySnapshot.forEach(function(doc) {
            console.log(doc.data());
            results.push(doc.data());
          });

          resolve(results[0]);
        } else {
          resolve(!querySnapshot.empty);
        }
      })
      .catch(function(error) {
        console.log("Error getting documents: ", error);
        reject(error);
      });
  });
};

export const checkUser = glassID => {
  return new Promise((resolve, reject) => {
    db.collection('users').where("glassID", "==", glassID)
      .get().then(array => {
        let results = [];
        array.forEach(datos=> {
          console.log(datos.data());
          results.push(datos.data());
        });
        resolve(results[0]);
      }).catch(err => {
        console.log(err)
        reject(err);
      })
  });
};

export const updateGlassID = (userID, glassID) => dispatch => {
  console.log(userID , 'iduser')
  console.log(glassID , 'glass')
  db.collection("users")
    .doc(userID)
    .set({ glassID }, { merge: true })
    .then(() => {
      console.log("Glass ID updated!");
      dispatch(setGlassID(glassID));
    })
    .catch(error => {
      console.error("Error updating glass ID: ", error);
    });
};

export const setGlassID = glassID => {
  return {
    type: SET_GLASS_ID,
    payload: {
      glassID
    }
  };
};

export const registerUser = (user, history) => async dispatch => {
  if (user.name !== null || user.email !== null || user.cc !== null || user.glassID !== null) {
    const exists = await checkIfUserExists(user.cc);

    if (!exists) {
      const newUser = {
        ...user,
        money: 0
      };

      db.collection("users")
        .doc(user.cc)
        .set(newUser)
        .then(() => {
          console.log("User successfully saved!");
          console.log(newUser);
          dispatch(setUser(newUser));
          history.push("/recarga");
        })
        .catch(error => {
          console.error("Error saving user: ", error);
        });
    } else {
        alert("Este usuario ya existe. Por favor recarga nuevamente su vaso");
        dispatch(setError("Este usuario ya existe. Por favor recarga nuevamente su vaso"));
    }
  } else {
    dispatch(setError("Ingrese todos los campos"));
  }
};

export const setUser = user => {
  return {
    type: SET_USER,
    payload: {
      user
    }
  };
};

export const updateMoney = (glassID, amount, totalNumber) => async dispatch => {
  const user = await checkIfGlassExists(glassID);
  console.log('en el servicio', totalNumber);
  const totalMoney = parseInt(user.money) + parseInt(amount);
  if (totalMoney >= 0) {
     bluetoothSerial = window.bluetoothSerial;
        let data = amount;
        if(bluetoothSerial){
          bluetoothSerial.write([data],function(a){
              console.log('si se envio ',  a)
          },function(b){
              console.log('no se envio ' ,  b)
          })
        }

    db.collection("users")
      .doc(user.cc)
      .set({ money: totalMoney, moneyView : totalNumber }, { merge: true })
      .then(() => {
        console.log("User's money updated!");
        dispatch(setMoney(totalMoney));
      })
      .catch(error => {
        console.error("Error updating user's money: ", error);
      });
  }
};

export const checkIfHasMoney = async (glassID, amount) => {
  const user = await checkIfGlassExists(glassID);

  // En este caso, el valor siempre tiene que llegar negativo (-)
  // Se hace la validación por si acaso

  let newAmount = amount;

  if (newAmount > 0) newAmount *= -1;

  const totalMoney = parseInt(user.money) + parseInt(newAmount);
  console.log(totalMoney);
  

  return totalMoney >= 0?true:false;
};

export const setMoney = money => {
  return {
    type: SET_MONEY,
    payload: {
      money
    }
  };
};
